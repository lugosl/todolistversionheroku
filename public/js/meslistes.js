var TodoApp = angular.module('TodoApp', []);

function MesListesController($scope, $http){
  $scope.formData = {};
  $scope.idUser = GetCookie("idUser");
  $scope.meslistes = null;
  $scope.idlisteselect = null;
  $scope.lestaches = null;
  $scope.ListeModification = -1;
  $scope.TacheModification = -1;

  if ($scope.idUser==null) document.location.href="./index.html";

  //ok
  $scope.recupererListes = function(){
    $scope.formData.idUser = $scope.idUser;
    $http.post('/getlisttask', $scope.formData)
        .success(function(data){
            $scope.meslistes = data;
            $scope.formData = {};
        })
        .error(function(data){
          console.log('Error: ' + data);
        });  
  };
  $scope.recupererListes();

  //ok
  $scope.ajouterListe = function(){
    if ($scope.formData.nomList!=null && $scope.formData.nomList.length>0){
        $scope.formData.idcreateurList = $scope.idUser;
        $scope.formData.idviewerList = [];
        $scope.formData.dateList = dateAujourdhui();

        $http.put('/createlisttask', $scope.formData)
            .success(function(data){
                $scope.formData = {};
                $scope.recupererListes();
            })
            .error(function(data){
              console.log('Error: ' + data);
            });  
    }
  };

  //ok
  $scope.afficherListe = function(index){
    $scope.ListeModification = -1;
    $scope.formData.page = "affichenom"; 
    $scope.formData.idUser = $scope.meslistes[index].idcreateurList;
    $http.post('/getusers', $scope.formData)
        .success(function(data){
            $scope.formData = {};
            $scope.meslistes[index].createur = data[0].pseudoUser;
            $scope.recupererTaches($scope.meslistes[index]._id);
            $scope.recupererUserPartage(index);
        })
        .error(function(data){
          console.log('Error: ' + data);
        });
  };
  //ok
  $scope.recupererCreateurTaches = function(){
    $scope.formData.page = "affichenom";
    
    $scope.lestaches.forEach(function(element) {
      $scope.formData.idUser = element.idcreateurTask;

      $http.post('/getusers', $scope.formData)
        .success(function(data){
            element.createur = data[0].pseudoUser;
        })
        .error(function(data){
          console.log('Error: ' + data);
        });
    });
  };
  //ok
  $scope.recupererUserPartage = function(index){
    $scope.formData.page = "affichenom";
    $scope.meslistes[index].viewer = [];

    $scope.meslistes[index].idviewerList.forEach(function(element) {
      $scope.formData.idUser = element;

      $http.post('/getusers', $scope.formData)
        .success(function(data){
            $scope.meslistes[index].viewer.push(data[0].pseudoUser);
        })
        .error(function(data){
          console.log('Error: ' + data);
        });
    });
  };
  //ok
  $scope.recupererTaches = function(id){
    $scope.formData.page = "meslistes"; 
    $scope.formData.idListTask = id;
    $scope.idlisteselect = id;

    $http.post('/gettasks', $scope.formData)
        .success(function(data){
            $scope.formData = {};
            $scope.lestaches = data;
            $scope.recupererCreateurTaches();
        })
        .error(function(data){
          console.log('Error: ' + data);
        });
  };
  //ok
  $scope.cacherTaches = function(){
    $scope.ListeModification = -1;
    $scope.idlisteselect = null;
    $scope.lestaches = null;
  };
  //ok
  $scope.supprimerListe = function(id){
    $http.delete('/deletelisttask/' + id)
        .success(function(data){
            $scope.formData = {};
        })
        .error(function(data){
          console.log('Error: ' + data);
        });
  }
  //ok
  $scope.seretirerListe = function(index){
    var idel = null;
    for (var i =0; i<$scope.meslistes[index].idviewerList.length && idel==null; i++){
      if ($scope.meslistes[index].idviewerList[i]==$scope.idUser){
        idel = i;
      }
    }
    $scope.meslistes[index].idviewerList.splice(idel, 1);
    $scope.formData.idviewerList = $scope.meslistes[index].idviewerList;
    $scope.formData.idList = $scope.meslistes[index]._id;

    $http.post('/updatelisttask', $scope.formData)
        .success(function(data){
            console.log(data);
            $scope.formData = {};
        })
        .error(function(data){
          console.log('Error: ' + data);
        }); 
  }
  //ok
  $scope.enleverListe = function(index){
    if ($scope.meslistes[index].idcreateurList==$scope.idUser){
        $scope.supprimerListe($scope.meslistes[index]._id);
    }
    else{
        $scope.seretirerListe(index);
    }
    $scope.recupererListes();
  };
  //ok
  $scope.ajouterTache = function(){
    $scope.formData.idcreateurTask = $scope.idUser;
    $scope.formData.idListTask = $scope.idlisteselect;
    $scope.formData.dateTask = dateAujourdhui();
    $scope.formData.doneTask = false;
    
    $http.put('/createtasks', $scope.formData)
        .success(function(data){
            $scope.formData = {};
            $scope.recupererTaches($scope.idlisteselect);
        })
        .error(function(data){
          console.log('Error: ' + data);
        }); 
  };
  //ok
  $scope.supprimerTache = function(index){
    $http.delete('/deletetasks/' + $scope.lestaches[index]._id)
        .success(function(data){
            $scope.formData = {};
            $scope.recupererTaches($scope.idlisteselect);
        })
        .error(function(data){
          console.log('Error: ' + data);
        }); 
  };
  //ok
  $scope.tacheStatus = function(index){
    $scope.formData.idTask = $scope.lestaches[index]._id;
    $scope.formData.doneTask = $scope.lestaches[index].doneTask;

    $http.post('/updatetasks',$scope.formData)
        .success(function(data){
            $scope.formData = {};
        })
        .error(function(data){
          console.log('Error: ' + data);
        }); 
  };
  //ok
  $scope.pasdansviewer = function(index, user){
    var liste = $scope.meslistes[index];

    if (user._id==liste.idcreateurList) return false;
    $scope.meslistes[index].idviewerList.forEach(function(element) {
      if (user._id==element) return false;
    });
    return true;
  }
  //ok
  $scope.partager = function(index){
      $scope.formData.page = "partage";
      var user = null;

      $http.post('/getusers', $scope.formData)
          .success(function(data){
             if (data!=null && data.length>0){
                user = data[0];
                if ($scope.pasdansviewer(index, user)){
                  $scope.meslistes[index].idviewerList.push(data[0]._id);
                  $scope.formData.idList = $scope.idlisteselect;
                  $scope.formData.idviewerList = $scope.meslistes[index].idviewerList;
          
                  $http.post('/updatelisttask',$scope.formData)
                    .success(function(data){
                        $scope.formData = {};
                        $scope.recupererUserPartage(index);
                    })
                    .error(function(data){
                      console.log('Error: ' + data);
                    });
                }
             }
             $scope.formData = {};
          })
          .error(function(data){
            console.log('Error: ' + data);
          });
  };
  //ok
  $scope.annulerChangement = function(){
    if ($scope.ListeModification>=0) $scope.ListeModification = -1;
    if ($scope.TacheModification>=0){
      $scope.TacheModification = -1;
    } 
  }
  //ok
  $scope.changerNomListe = function(index){
    $scope.ListeModification = index;
    $scope.idlisteselect = null;
  }
  //ok
  $scope.modifierNomListe = function(index){
    if ($scope.meslistes[index].nomList!=null && $scope.meslistes[index].nomList.length>0){
      $scope.formData2 = {};
      $scope.formData2.idList = $scope.meslistes[index]._id;
      $scope.formData2.nomList = $scope.meslistes[index].nomList;

      $http.post('/updatelisttask',$scope.formData2)
      .success(function(data){
          $scope.formData2 = {};
          $scope.ListeModification = -1;
      })
      .error(function(data){
        console.log('Error: ' + data);
      });
    }
  }
  //ok
  $scope.changerNomTache = function(index){
    $scope.TacheModification = index;
  }
  //TODO
  $scope.modifierNomTache = function(index){
    if ($scope.lestaches[index].nomTask!=null && $scope.lestaches[index].nomTask.length>0){
      $scope.formData2 = {};
      $scope.formData2.idTask = $scope.lestaches[index]._id;
      $scope.formData2.nomTask = $scope.lestaches[index].nomTask;
      $scope.formData2.idcreateurTask = $scope.idUser;

      $http.post('/updatetasks',$scope.formData2)
      .success(function(data){
          $scope.formData2 = {};
          $scope.lestaches[index].idcreateurTask = $scope.idUser;
          $scope.recupererCreateurTaches();
          $scope.TacheModification = -1;
      })
      .error(function(data){
        console.log('Error: ' + data);
      });
    }
  }
}