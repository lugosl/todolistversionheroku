var MongoClient = require('mongodb').MongoClient;
var uri = "mongodb+srv://admin:polytech@cluster0-fvimi.mongodb.net/Poly?retryWrites=true";
var client = new MongoClient(uri, {useNewUrlParser:true });
var db;

/*
    users :
        nomUser
        prenomUser
        emailUser
        pseudoUser
        mdpUser

    listtask :
        nomList
        idcreateurList
        dateList
        idviewerList

    tasks :
        nomTask
        idcreateurTask
        idListTask
        dateTask
        doneTask
*/


var datalayer = {
    init : function(cb){
        //Initialize connection once
        client.connect(function(err) {
            if(err) throw err;
            db = client.db("Poly");
            cb();
        });
    },
    get : function(collection, document, cb){
        if (document._id!=null){
            ObjectID = require('mongodb').ObjectID;
            document = {
                _id : new ObjectID(document._id)
            };
        }
        db.collection(collection).find(document).toArray(function(err, docs) {
            cb(docs);
        });
    },
    create : function(collection, document, cb){
        db.collection(collection).insertOne(document, function() {
            cb({success : true});
        });
    },
    update : function(collection, id, document, cb){
        ObjectID = require('mongodb').ObjectID;
        var ident = {
            _id : new ObjectID(id)
        };
        
    console.log(ident);
    console.log(document);
        db.collection(collection).updateOne(ident, {$set: document}, function(err, result) {
            cb({success : true});
        });        
    },
    delete : function(collection, id, cb){
        ObjectID = require('mongodb').ObjectID;
        var ident = {
            _id : new ObjectID(id)
        };
        console.log(ident);
        db.collection(collection).deleteOne(ident, function(err, result) {
            cb({success : true});
        });        
    }
};
module.exports=datalayer;