
var datalayer = require('./datalayer.js');

var express = require('express');
var app = express();
var port = process.env.PORT;
var morgan = require('morgan');
var bodyParser = require('body-parser');
var route = require('./api-route');


app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
route(app);

datalayer.init(function(){
    console.log('init');
    app.listen(port);
    console.log("Listening on port "+port);
});
