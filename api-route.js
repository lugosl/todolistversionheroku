var datalayer = require('./datalayer.js');

var route = function(app){
    app.get('/', function(req, res){
        res.sendFile('./public/index.html');
    });

/* users */
  //read OK
  app.post('/getusers', function(req, res){
    var collection = "users";
    var document = {};
    var page = req.body.page;

    if (page=="connection"){
      document.pseudoUser = req.body.pseudoUser;
      document.mdpUser = req.body.mdpUser; 
    }
    if (page=="inscription"){
      if (req.body.inputInscription=="pseudo") document.pseudoUser = req.body.pseudoUser;
      else document.emailUser = req.body.emailUser;
    }
    if (page=="affichenom"){
      document._id = req.body.idUser;
    } 
    if (page=="partage"){
      document = {$or: [
          {pseudoUser: req.body.UserPartage},
          {emailUser: req.body.UserPartage}
      ]};
    }
    datalayer.get(collection, document, function(dtSet){
      res.send(dtSet);
    });
  });
  //create OK
  app.put('/createusers', function(req, res){
    var collection = "users";
    var document = {      
      nomUser : req.body.nomUser,
      prenomUser : req.body.prenomUser,
      emailUser : req.body.emailUser,
      pseudoUser : req.body.pseudoUser,
      mdpUser : req.body.mdpUser
    }    
    datalayer.create(collection, document, function(dtSet){
      res.send(dtSet);
    });    
  });
  //update OK
  app.post('/updateusers', function(req, res){
    var collection = "users";
    var id = req.body.idUser;
    var document = {}

    if (req.body.nomUser!=null) document.nomUser = req.body.nomUser;
    if (req.body.prenomUser!=null) document.prenomUser = req.body.prenomUser;
    if (req.body.emailUser!=null) document.emailUser = req.body.emailUser;
    if (req.body.pseudoUser!=null) document.pseudoUser = req.body.pseudoUser;
    if (req.body.mdpUser!=null) document.mdpUser = req.body.mdpUser;
    
    datalayer.update(collection, id, document, function(dtSet){
      res.send(dtSet);
    }); 
  });
  //delete OK
  app.delete('/deleteusers', function(req, res){
    var collection = "users";
    var id = req.body.idUser;
    
    datalayer.delete(collection, id, function(dtSet){
      res.send(dtSet);
    });
  });

/* listtask */
  //read OK
  app.post('/getlisttask', function(req, res){
    var collection = "listtask";
    var document = {$or: [
        {idcreateurList: req.body.idUser},
        {idviewerList: req.body.idUser}
    ]};
    
    datalayer.get(collection, document, function(dtSet){
      res.send(dtSet);
    });
  });
  //create OK
  app.put('/createlisttask', function(req, res){
    var collection = "listtask";
    var document = {
      nomList : req.body.nomList,
      idcreateurList : req.body.idcreateurList,
      dateList : req.body.dateList,
      idviewerList : req.body.idviewerList
    }

    datalayer.create(collection, document, function(dtSet){
      res.send(dtSet);
    });    
  });
  //update OK
  app.post('/updatelisttask', function(req, res){
    var collection = "listtask";
    var id = req.body.idList;
    var document = {};

    if (req.body.nomList!= null) document.nomList = req.body.nomList;
    if (req.body.idcreateurList!= null) document.idcreateurList = req.body.idcreateurList;
    if (req.body.dateList!= null) document.dateList = req.body.dateList;
    if (req.body.idviewerList!= null) document.idviewerList = req.body.idviewerList;
    
    console.log(document);
    datalayer.update(collection, id, document, function(dtSet){
      res.send(dtSet);
    }); 
  });
  //delete OK
  app.delete('/deletelisttask/:id', function(req, res){
    var collection = "listtask";
    var id = req.params.id;
    
    console.log("id");
    console.log(id);
    datalayer.delete(collection, id, function(dtSet){
      res.send(dtSet);
    });
  });

/* tasks */
  //read OK
  app.post('/gettasks', function(req, res){
    var collection = "tasks";
    var document = {};    
    var page = req.body.page;

    if (page=="meslistes"){
      document.idListTask = req.body.idListTask;
    }
    if (page=="task"){
      document._id = req.body.idTask;
    }
    
    datalayer.get(collection, document, function(dtSet){
      res.send(dtSet);
    });
  });
  //create OK
  app.put('/createtasks', function(req, res){
    var collection = "tasks";
    var document = {
      nomTask : req.body.nomTask,
      idcreateurTask : req.body.idcreateurTask,
      idListTask : req.body.idListTask,
      dateTask : req.body.dateTask,
      doneTask : (/^true$/i).test(req.body.doneTask)
    }
    
    datalayer.create(collection, document, function(dtSet){
      res.send(dtSet);
    });    
  });
  //update OK
  app.post('/updatetasks', function(req, res){
    var collection = "tasks";
    var id = req.body.idTask;
    var document = {};

    if (req.body.nomTask!=null) document.nomTask = req.body.nomTask;
    if (req.body.idcreateurTask!=null) document.idcreateurTask = req.body.idcreateurTask;
    if (req.body.idListTask!=null) document.idListTask = req.body.idListTask;
    if (req.body.dateTask!=null) document.dateTask = req.body.dateTask;
    if (req.body.doneTask!=null) document.doneTask = req.body.doneTask;
    
    datalayer.update(collection, id, document, function(dtSet){
      res.send(dtSet);
    }); 
  });
  //delete OK
  app.delete('/deletetasks/:id', function(req, res){
    var collection = "tasks";
    var id = req.params.id;
    
    datalayer.delete(collection, id, function(dtSet){
      res.send(dtSet);
    });
  });
}
module.exports = route;