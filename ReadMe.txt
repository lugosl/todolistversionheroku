TODOLIST

_Login
_Ajoute Liste de taches
_Afficher une liste de tâches
_Ajoute une tache (à une liste) à une base de données (en ligne)
_Affiche les taches avec la date de création
_Possibilité de modifier une tache ainsi que de la marquer comme faite (barre la tache)
_______________________________________________

EXECUTION :

_Se placer dans le dossier contenant server.js
_nodemon server.js
_Ouvir dans un navigateur localhost:port
_______________________________________________

Accessible via 
https://ancient-plains-95165.herokuapp.com
